package com.jira59160548.fishbook.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface FishDatabaseDao {
    @Insert
    fun insert(fish: FishData)

    @Update
    fun update(fish: FishData)

    @Query("SELECT * from fish_data_table WHERE fishId = :key")
    fun get(key: Long): FishData

    @Query("SELECT * FROM fish_data_table ORDER BY fishId DESC")
    fun getAllFishData(): LiveData<List<FishData>>

    @Query("SELECT * FROM fish_data_table ORDER BY fishId DESC LIMIT 1")
    fun getFishData(): FishData?

    @Query("SELECT * from fish_data_table WHERE fishId = :key")
    fun getFishWithId(key: Long): LiveData<FishData>

    @Delete
    fun delete(fish: FishData)
}