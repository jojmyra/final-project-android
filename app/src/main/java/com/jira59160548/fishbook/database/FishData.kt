package com.jira59160548.fishbook.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "fish_data_table")
data class FishData(
    @PrimaryKey(autoGenerate = true)
    var fishId: Long = 0L,

    @ColumnInfo(name = "fish_name")
    var fishName: String = "unknown",

    @ColumnInfo(name = "fish_type")
    var fishType: String = "unknown",

    @ColumnInfo(name = "fish_calories")
    var fishCalories: Int = 0,

    @ColumnInfo(name = "fish_detail")
    var fishDetail: String = "unknown"

) {
    override fun toString(): String {
        return "$fishName"
    }
}