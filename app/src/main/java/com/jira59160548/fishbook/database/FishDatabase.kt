package com.jira59160548.fishbook.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [FishData::class], version = 1, exportSchema = true)
abstract class FishDatabase : RoomDatabase() {
    abstract val fishDatabaseDao: FishDatabaseDao

    companion object {
        @Volatile
        private var INSTANCE: FishDatabase? = null

        fun getInstance(context: Context): FishDatabase {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        FishDatabase::class.java,
                        "fish_database"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}