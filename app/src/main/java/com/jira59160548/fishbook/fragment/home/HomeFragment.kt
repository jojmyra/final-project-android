package com.jira59160548.fishbook.fragment.home

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.ItemTouchHelper.*
import com.jira59160548.fishbook.R
import com.jira59160548.fishbook.database.FishDatabase
import com.jira59160548.fishbook.databinding.FragmentHomeBinding
import androidx.recyclerview.widget.RecyclerView



class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding: FragmentHomeBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_home, container, false)

        val application = requireNotNull(this.activity).application
        val dataSource = FishDatabase.getInstance(application).fishDatabaseDao
        val viewModelFactory = HomeViewModelFactory(dataSource)

        val homeViewModel = ViewModelProviders
            .of(this, viewModelFactory).get(HomeViewModel::class.java)

        binding.lifecycleOwner = this

        binding.btnAdd.setOnClickListener {
            it.findNavController().navigate(R.id.action_homeFragment_to_addDataFragment)
        }

        binding.homeViewModel = homeViewModel

        val adapter = FishDataAdapter(FishListener { fish ->
            Toast.makeText(context, "${fish}", Toast.LENGTH_LONG).show()
            homeViewModel.onFishDataClicked(fish)
        })
        binding.fishList.adapter = adapter

        homeViewModel.fishAll.observe(this, Observer {
            it?.let {
                adapter.submitList(it)
            }
        })

        homeViewModel.navigateToDetail.observe(this, Observer { fish ->
            fish?.let {
                this.findNavController().navigate(
                    HomeFragmentDirections
                        .actionHomeFragmentToDetailFragment(fish.fishId, fish.fishName))
                homeViewModel.onSleppFishDataNavigated()
            }
        })

        ItemTouchHelper(object : SimpleCallback(
            0,
            LEFT or RIGHT
        ) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                homeViewModel.onDelete(adapter.getFishDataAt(viewHolder.adapterPosition))
                Toast.makeText(context, "Fish deleted", Toast.LENGTH_SHORT).show()
            }
        }).attachToRecyclerView(binding.fishList)

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.options_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(
            item,
            view!!.findNavController())
                || super.onOptionsItemSelected(item)
    }

}
