package com.jira59160548.fishbook.fragment.addData

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jira59160548.fishbook.database.FishDatabaseDao
import java.lang.IllegalArgumentException

class AddDataViewModelFactory(
    private val dataSource: FishDatabaseDao,
    private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AddDataViewModel::class.java)) {
            return AddDataViewModel(dataSource, application) as T
        }
        throw IllegalArgumentException("Unknow View Model Class")
    }
}