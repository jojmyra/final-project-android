package com.jira59160548.fishbook.fragment.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jira59160548.fishbook.database.FishData
import com.jira59160548.fishbook.database.FishDatabaseDao
import kotlinx.coroutines.*
import timber.log.Timber

class HomeViewModel(
    datasource: FishDatabaseDao
): ViewModel() {

    val database = datasource

    private var viewModelJob = Job()
    private var uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private var fish = MutableLiveData<FishData?>()

    val fishAll = database.getAllFishData()

    private val _navigateToDetail = MutableLiveData<FishData>()
    val navigateToDetail
        get() = _navigateToDetail

    fun onFishDataClicked(fish: FishData) {
        _navigateToDetail.value = fish
    }

    fun onSleppFishDataNavigated() {
        _navigateToDetail.value = null
    }

    fun onDelete(fish: FishData) {
        uiScope.launch {
            delete(fish)
        }
    }

    init {
        Timber.i("FishViewModel Initialized")
        initializeFish()
    }

    private fun initializeFish() {
        uiScope.launch {
            fish.value = getFishFromDatabase()
        }
    }

    private suspend fun getFishFromDatabase(): FishData? {
        return withContext(Dispatchers.IO) {
            var fish = database.getFishData()
            Timber.i("FishData $fish")
            fish
        }
    }

    private suspend fun delete(fish: FishData) {
        withContext(Dispatchers.IO) {
            database.delete(fish)
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }


}