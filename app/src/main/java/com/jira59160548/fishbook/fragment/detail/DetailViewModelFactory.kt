package com.jira59160548.fishbook.fragment.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jira59160548.fishbook.database.FishDatabaseDao
import java.lang.IllegalArgumentException

class DetailViewModelFactory(
    private val fishKey: Long,
    private val dataSource: FishDatabaseDao
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DetailViewModel::class.java)) {
            return DetailViewModel(fishKey, dataSource) as T
        }
        throw IllegalArgumentException("Unknow View Model Class")
    }
}