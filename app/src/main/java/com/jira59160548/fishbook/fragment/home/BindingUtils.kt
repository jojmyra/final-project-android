package com.jira59160548.fishbook.fragment.home

import android.annotation.SuppressLint
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.jira59160548.fishbook.R
import com.jira59160548.fishbook.database.FishData


@BindingAdapter("fishName")
fun TextView.setFishNameTxt(item: FishData?) {
    text = item?.fishName
}

@BindingAdapter("fishImage")
fun ImageView.setFishImage(item: FishData?) {
    setImageResource(when (item!!.fishType) {
        "ปลาน้ำจืด" -> R.drawable.fish1
        "ปลาน้ำเค็ม" -> R.drawable.fish2
        "ปลาน้ำกร่อย" -> R.drawable.fish3
        else -> R.drawable.unknow
    })
}

@SuppressLint("SetTextI18n")
@BindingAdapter("fishType")
fun TextView.setFishTypeTxt(item: FishData?) {
    text = "ชนิดของปลา: ${item?.fishType}"
}

@SuppressLint("SetTextI18n")
@BindingAdapter("fishCalories")
fun TextView.setFishCaloriesTxt(item: FishData?) {
    item?.let {
        text = "แคลอรี่ของปลา: ${item.fishCalories}"
    }
}

@SuppressLint("SetTextI18n")
@BindingAdapter("fishDetail")
fun TextView.setFishDetailTxt(item: FishData?) {
    text = "รายละเอียดของปลา:\n${item?.fishDetail}"
}