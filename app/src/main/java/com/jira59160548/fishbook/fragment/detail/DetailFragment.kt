package com.jira59160548.fishbook.fragment.detail

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.jira59160548.fishbook.R
import com.jira59160548.fishbook.database.FishDatabase
import com.jira59160548.fishbook.databinding.FragmentDetailBinding

class DetailFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentDetailBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_detail, container, false)
        val application = requireNotNull(this.activity).application
        val arguments = arguments?.let { DetailFragmentArgs.fromBundle(it) }

        val dataSource = FishDatabase.getInstance(application).fishDatabaseDao
        val viewModelFactory = DetailViewModelFactory(arguments!!.fishKey, dataSource)

        val detailViewModel = ViewModelProviders.of(
            this, viewModelFactory).get(DetailViewModel::class.java)

        binding.detailViewModel = detailViewModel

        binding.lifecycleOwner = this

        detailViewModel.navigateToHome.observe(this, Observer {
            if (it == true) {
                this.findNavController().navigate(
                    DetailFragmentDirections.actionDetailFragmentToHomeFragment())

                detailViewModel.doneNavigating()
            }
        })

        detailViewModel.onShare.observe(this, Observer {
            if (it == true) {
                detailViewModel.onDoneShared()
                val shareIntent: Intent = Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(Intent.EXTRA_SUBJECT, "${detailViewModel.getFish().value?.fishName} - ${detailViewModel.getFish().value?.fishDetail}")
                    type = "text/plain"
                }
                startActivity(Intent.createChooser(shareIntent, "Share using:"))
            }
        })


        // Inflate the layout for this fragment
        return binding.root
    }

}
