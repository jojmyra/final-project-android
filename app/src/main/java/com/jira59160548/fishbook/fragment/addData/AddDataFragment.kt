package com.jira59160548.fishbook.fragment.addData

import android.app.Application
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.room.RoomDatabase
import com.jira59160548.fishbook.R
import com.jira59160548.fishbook.database.FishData
import com.jira59160548.fishbook.database.FishDatabase
import com.jira59160548.fishbook.database.FishDatabaseDao
import com.jira59160548.fishbook.databinding.FragmentAddDataBinding
import kotlinx.coroutines.*
import timber.log.Timber

class AddDataFragment : Fragment() {



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentAddDataBinding>(
            inflater,
            R.layout.fragment_add_data, container, false
        )

        val application = requireNotNull(this.activity).application
        val dataSource = FishDatabase.getInstance(application).fishDatabaseDao
        val viewModelFactory = AddDataViewModelFactory(dataSource,application)
        val viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(AddDataViewModel::class.java)
        viewModel.addCompleted.observe(this, Observer {
            if (it) {
                Toast.makeText(activity, "เพิ่มข้อมูลสำเร็จ", Toast.LENGTH_SHORT).show()
                findNavController().navigate(
                    AddDataFragmentDirections.actionAddDataFragmentToHomeFragment())
            }
        })
        binding.addDataViewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onDestroy() {
        Timber.i("AddData onDestroy")
        super.onDestroy()
    }

    override fun onResume() {
        Timber.i("AddData onResume")
        super.onResume()
    }

    override fun onPause() {
        Timber.i("AddData onPause")
        super.onPause()
    }

}
