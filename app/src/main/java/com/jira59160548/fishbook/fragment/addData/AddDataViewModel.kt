package com.jira59160548.fishbook.fragment.addData

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jira59160548.fishbook.R
import com.jira59160548.fishbook.database.FishData
import com.jira59160548.fishbook.database.FishDatabaseDao
import kotlinx.coroutines.*
import timber.log.Timber

class AddDataViewModel(
    datasource: FishDatabaseDao,
    application: Application
) : ViewModel() {

    val database = datasource
    private var viewModelJob = Job()
    private var uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)


    var fishName = MutableLiveData<String>()
    var fishType = MutableLiveData<Int>()
    var fishCalories = MutableLiveData<String>()
    var fishDetail = MutableLiveData<String>()

    var alert = MutableLiveData<String>()

    var fishTypeArray: Array<String> = application.resources.getStringArray(R.array.fishType)

    private val _addCompleted = MutableLiveData<Boolean>()
    val addCompleted: LiveData<Boolean>
        get() = _addCompleted

    init {
        Timber.i("AddDataViewModel Created!")
        alert.value = ""
    }

    fun onAdd() {
        uiScope.launch {
            if (checkEmpty()) alert.value = "กรุณากรอกข้อมูลให้ครบถ้วน"
            else {
                val fish = FishData()
                fish.fishName = fishName.value!!
                fish.fishType = fishTypeArray[fishType.value!!]
                fish.fishCalories = fishCalories.value!!.toInt()
                fish.fishDetail = fishDetail.value!!
                insert(fish)
                _addCompleted.value = true
            }
        }
    }

    private fun checkEmpty(): Boolean {
        return (fishName.value == null || fishCalories.value == null || fishDetail.value == null)
    }

    private suspend fun insert(fish: FishData) {
        withContext(Dispatchers.IO) {
            database.insert(fish)
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

}