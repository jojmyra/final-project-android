package com.jira59160548.fishbook.fragment.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jira59160548.fishbook.database.FishDatabaseDao
import java.lang.IllegalArgumentException

class HomeViewModelFactory(
    private val dataSource: FishDatabaseDao
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {
            return HomeViewModel(dataSource) as T
        }
        throw IllegalArgumentException("Unknow View Model Class")
    }
}