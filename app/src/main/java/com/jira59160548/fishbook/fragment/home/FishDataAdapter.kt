package com.jira59160548.fishbook.fragment.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jira59160548.fishbook.database.FishData
import com.jira59160548.fishbook.databinding.MenuHomeBinding

class FishDataAdapter(private val clickListener: FishListener) : ListAdapter<FishData, FishDataAdapter.ViewHolder>(FishDataCallBack()) {


    class ViewHolder private constructor(val binding: MenuHomeBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(
            item: FishData,
            clickListener: FishListener
        ) {
            binding.fish = item
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = MenuHomeBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, clickListener)
    }

    fun getFishDataAt(position: Int): FishData {
        return getItem(position)
    }

}

class FishDataCallBack : DiffUtil.ItemCallback<FishData>() {
    override fun areItemsTheSame(oldItem: FishData, newItem: FishData): Boolean {
        return oldItem.fishId == newItem.fishId
    }

    override fun areContentsTheSame(oldItem: FishData, newItem: FishData): Boolean {
        return oldItem == newItem
    }

}

class FishListener(val clickListener: (sleepId: FishData) -> Unit) {
    fun onClick(fish: FishData) = clickListener(fish)
}