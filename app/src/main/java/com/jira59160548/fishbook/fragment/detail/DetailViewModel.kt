package com.jira59160548.fishbook.fragment.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jira59160548.fishbook.database.FishData
import com.jira59160548.fishbook.database.FishDatabaseDao
import kotlinx.coroutines.*

class DetailViewModel(
    private val fishKey: Long = 0L,
    datasource: FishDatabaseDao
) : ViewModel() {

    val database = datasource

    private var viewModelJob = Job()
    private val fish: LiveData<FishData>
    fun getFish() = fish

    private val _onShare = MutableLiveData<Boolean>()
    val onShare: LiveData<Boolean>
        get() = _onShare


    init {
        fish = database.getFishWithId(fishKey)
    }

    private val _navigateToHome = MutableLiveData<Boolean?>()
    val navigateToHome: LiveData<Boolean?>
        get() = _navigateToHome

    fun doneNavigating() {
        _navigateToHome.value = null
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun onShared() {
        _onShare.value = true
    }

    fun onDoneShared() {
        _onShare.value = false
    }


}